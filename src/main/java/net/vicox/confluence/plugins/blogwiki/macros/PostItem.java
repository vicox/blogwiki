package net.vicox.confluence.plugins.blogwiki.macros;

import com.atlassian.confluence.core.SpaceContentEntityObject;

/**
 * Used by the blogwiki-posts macro for rendering new posts.
 *
 * @author Georg Schmidl <georg.schmidl@vicox.net>
 */
public class PostItem {

    SpaceContentEntityObject post;
    String renderedHtml;

    public PostItem(SpaceContentEntityObject post, String renderedHtml) {
        this.post = post;
        this.renderedHtml = renderedHtml;
    }

    public SpaceContentEntityObject getPost() {
        return post;
    }

    public void setPost(SpaceContentEntityObject post) {
        this.post = post;
    }

    public String getRenderedHtml() {
        return renderedHtml;
    }

    public void setRenderedHtml(String renderedHtml) {
        this.renderedHtml = renderedHtml;
    }
}
