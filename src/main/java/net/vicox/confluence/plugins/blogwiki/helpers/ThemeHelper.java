package net.vicox.confluence.plugins.blogwiki.helpers;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;

import java.util.List;

/**
 * Blogwiki velocity helper.
 *
 * @author Georg Schmidl <georg.schmidl@vicox.net>
 */
public class ThemeHelper {

    private SpaceManager spaceManager;

    public List<Space> getSpaces() {
        return spaceManager.getAllSpaces();
    }

    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }
}
