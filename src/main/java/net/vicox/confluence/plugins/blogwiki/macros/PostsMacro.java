package net.vicox.confluence.plugins.blogwiki.macros;

import com.atlassian.bonnie.AnyTypeObjectDao;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.macro.query.BooleanQueryFactory;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.service.SpaceCategoryEnum;
import com.atlassian.confluence.search.v2.*;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.SpaceCategoryQuery;
import com.atlassian.confluence.search.v2.query.TextFieldQuery;
import com.atlassian.confluence.search.v2.searchfilter.SpacePermissionsSearchFilter;
import com.atlassian.confluence.search.v2.sort.CreatedSort;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Renders the resent 5 added pages or blogposts.
 *
 * @author Georg Schmidl <georg.schmidl@vicox.net>
 */
public class PostsMacro extends BaseMacro {

    private static int POSTS_MAX = 10;

    protected SearchManager searchManager;
    protected AnyTypeObjectDao anyTypeObjectDao;
    protected Renderer viewRenderer;

    @Override
    public String execute(Map map, String s, RenderContext renderContext) throws MacroException {
        BooleanQueryFactory query = new BooleanQueryFactory();

        BooleanQueryFactory contentTypequery = new BooleanQueryFactory();
        contentTypequery.addShould(new ContentTypeQuery(ContentTypeEnum.BLOG));
        contentTypequery.addShould(new ContentTypeQuery(ContentTypeEnum.PAGE));

        query.addMust(contentTypequery);
        query.addMust(new SpaceCategoryQuery(SpaceCategoryEnum.GLOBAL));
        query.addMustNot(new TextFieldQuery("title", "Home", BooleanOperator.AND));

        SearchSort searchSort = new CreatedSort(SearchSort.Order.DESCENDING);

        ISearch search = new ContentSearch(query.toBooleanQuery(),
                searchSort, SpacePermissionsSearchFilter.getInstance(), null);

        SearchResults searchResults = null;
        try {
            searchResults = searchManager.search(search);
        } catch (InvalidSearchException e) {
            throw new MacroException(e);
        }

        List<PostItem> postItems = new ArrayList<PostItem>();

        int postCount = 0;
        boolean lastPostWasBookmark = false;
        for (Iterator<SearchResult> iterator = searchResults.iterator(); iterator.hasNext(); ) {
            SpaceContentEntityObject contentEntityObject = (SpaceContentEntityObject)
                    anyTypeObjectDao.findByHandle(iterator.next().getHandle());
            if(contentEntityObject != null) {
                boolean isBookMark = isBookmark(contentEntityObject);

                if(postCount > POSTS_MAX - 1 && !(lastPostWasBookmark && isBookMark)) {
                    break;
                }

                String renderedHtml = viewRenderer.render(contentEntityObject);
                postItems.add(new PostItem(contentEntityObject, renderedHtml));

                if (!isBookMark || (isBookMark && !lastPostWasBookmark)) {
                    postCount += 1;
                }
                lastPostWasBookmark = isBookMark;
            }
        }

        Map<String, Object> context = MacroUtils.defaultVelocityContext();
        context.put("postItems", postItems);

        return VelocityUtils.getRenderedTemplate("macros/posts.vm", context);
    }

    private boolean isBookmark(SpaceContentEntityObject contentEntityObject) {
        return contentEntityObject instanceof Page
                && ((Page) contentEntityObject).getParent() != null
                && ((Page) contentEntityObject).getParent().getTitle().equals(".bookmarks");
    }

    public void setSearchManager(SearchManager searchManager) {
        this.searchManager = searchManager;
    }

    public void setAnyTypeObjectDao(AnyTypeObjectDao anyTypeObjectDao) {
        this.anyTypeObjectDao = anyTypeObjectDao;
    }

    public void setViewRenderer(Renderer viewRenderer) {
        this.viewRenderer = viewRenderer;
    }

    @Override
    public boolean hasBody() {
        return false;
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.ALL;
    }
}
